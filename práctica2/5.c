#include <stdio.h> 
#include <stdlib.h> 
#include <pthread.h>
#include <string.h>
#include <unistd.h>
//#include <time.h>

#define NUM_HILOS 3

typedef struct parametros{

	int indice;
	int *vector_fila;
	int *vector_columna;

}params;

void *MultiplicarMatrices(params *parametros)
{
	int *suma=(int *)malloc(sizeof(int));
	int i;
	*suma=0;
	int aux=0;

		printf("\n\nLa fila a multiplicar es -> \n");
		for(i=0;i<3;i++)
			printf(" [%d] * [%d]  \n",parametros->vector_fila[i], parametros->vector_columna[i]);

		for(i=0;i<3;i++)
		{
			aux = parametros->vector_fila[i] * parametros->vector_columna[i];
			(*suma)+= aux;
		}

	pthread_exit(suma);
}

int main(int argc, char ** argv)
{
	int m[3][3] = {{3, 1, 0},{-1, 7, 2},{4, 2, 1}};
	int v[3] = {3,0,1};
	int v_resu[3];
	int i;
	int *suma;
	pthread_t thd1[10];

	params parametros[NUM_HILOS];

	for(i=0;i<NUM_HILOS;i++)
	{
		parametros[i].indice = i;
		parametros[i].vector_fila=&m[i][0];
		parametros[i].vector_columna=v;

		pthread_create (&(thd1[i]), NULL,(void *)MultiplicarMatrices,(void *)&parametros[i]);	
	}
	
	for(i=0;i<NUM_HILOS;i++)
	{
		pthread_join(thd1[i],(void **)&suma);
		v_resu[i]=*suma;
	}


	printf("\n\nEl vector resultante es: -> ");
	for(i=0;i<3;i++)
	{
		printf(" %d ",v_resu[i]);
	}

	printf("\nHan finalizado los thread.\n");
	exit(EXIT_SUCCESS);
}