#include <stdio.h> 
#include <stdlib.h> 
#include <pthread.h>
#include <string.h>
#include <unistd.h>

void *mostrarCaracter(void *cadena)
{
	int i;	
	char *cad = (char *)cadena;
	for(i=0;i<strlen(cad);i++)
	{
		printf("%c\n",cad[i]);
		sleep(1);
	}
    
	pthread_exit(NULL);
}

int main(int argc, char *argv[])
{	

	pthread_t thd1, thd2;
	char cadena1[10]="Hola";
	char cadena2[10]=" Mundo";
	
	pthread_create (&thd1, NULL, mostrarCaracter, (void *)cadena1); 
	pthread_create (&thd2, NULL, mostrarCaracter, (void *)cadena2);

	pthread_join(thd1, NULL);	 
	pthread_join(thd2, NULL);

	printf("\nHan finalizado los thread.\n");
	exit(EXIT_SUCCESS); 	
}