#include <pthread.h> 
#include <semaphore.h>
#include <errno.h>
#include <stdio.h> 
#include <stdlib.h>

#define num_hilos 300
#define iteraciones 1000

int global=0;

int *contar()
{
	int i;

	for(i=0;i<iteraciones;i++)
		global++;

	pthread_exit(NULL);
}

int main(int argc, char ** argv)
{
	pthread_t thd[num_hilos];	
	int i;
	int *contador;
	int total=0;

	for(i=0;i<num_hilos;i++)
	{
		pthread_create(&(thd[i]), NULL, (void *)contar, NULL);
	}

	for(i=0;i<num_hilos;i++)
	{
		pthread_join(thd[i],NULL);
	}

	printf("\n\nNumero de Hilos %d\nRealizan %d iteraciones cada hilo,\nEl RESULTADO total es:-> %d\n\n",num_hilos,iteraciones,global);

	printf("\nHan finalizado los thread.\n");
	exit(EXIT_SUCCESS);
}