#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define NUM_THD 1
#define MAX_SIZE 5
#define MAX_COUNT MAX_SIZE-1
#define NUM_PROD 10

pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
int buffer[MAX_SIZE];
int productos_disponibles = 0;

void * manejadorProductor(void * param)
{
	int num_items = 0;
	int * index = (int*)param;
	int pos_buffer = 0;

	

	while(num_items < NUM_PROD)
	{
		usleep(rand() % 1000000);
		pthread_mutex_lock(&mtx);

		if(productos_disponibles < MAX_SIZE)
		{
			buffer[pos_buffer] = rand() % 10 + 1;
			printf("PRODUCIDO un %d\t(%d)\tHILO: %d\n", buffer[pos_buffer], num_items+1, *index);
			pos_buffer = (pos_buffer + 1) % MAX_SIZE;
			productos_disponibles++;
			num_items++;
		}

		pthread_mutex_unlock(&mtx);
	}

	printf("Ya se produjeron todos los productos.\n");

	pthread_exit(NULL);
}

void * manejadorConsumidor(void * param)
{
	int num_items = 0;
	int * index = (int *)param;
	int pos_buffer = 0;

	

	while(num_items < NUM_PROD)
	{
		
		usleep(rand() % 1000000);
		pthread_mutex_lock(&mtx);

		if(productos_disponibles > 0)
		{
			productos_disponibles--;
			printf("Consumido un %d por el consumidor %d\t(%d).\t Unidades disponibles: %d\n", buffer[pos_buffer], *index, num_items, productos_disponibles);
			
			num_items++;
			pos_buffer = (pos_buffer + 1) % MAX_SIZE;
		}

		pthread_mutex_unlock(&mtx);
	}

	printf("Ya se consumieron todos los productos.\n");

	pthread_exit(NULL);
}


int main()
{
	pthread_t thdProductor[NUM_THD];
	pthread_t thdConsumidor[NUM_THD];
	int i, indices[NUM_THD];

	srand(time(NULL));

	for(i = 0; i < NUM_THD; i++)
	{
		indices[i] = i;
		pthread_create(&(thdProductor[i]), NULL, manejadorProductor, &(indices[i]));
		pthread_create(&(thdConsumidor[i]), NULL,manejadorConsumidor, &(indices[i]));

	}

	for(i = 0; i < NUM_THD; i++)
	{		
		pthread_join(thdProductor[i], NULL);
		pthread_join(thdConsumidor[i], NULL);
	}


	pthread_mutex_destroy(&mtx);
	pthread_exit(NULL);
}