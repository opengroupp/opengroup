#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>

#define NUMCLI 3
#define NUMPRO 5

int camisetas[NUMPRO] = { 100,100,100,100,100 };
pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;

void * manejadorCliente()
{
	int numCam;
	int cantCam;

	numCam = rand()%5;

	pthread_mutex_lock(&mtx);

	cantCam = rand()%camisetas[numCam] + 1;

	camisetas[numCam] -= cantCam;

	printf("Consumidas %d camisetas del tipo %d.\n", cantCam, numCam);

	pthread_mutex_unlock(&mtx);

	pthread_exit(NULL);

}

void * manejadorProveedor()
{
	int numCam;
	int cantCam;

	numCam = rand()%5;

	cantCam = rand()%20 + 1;

	pthread_mutex_lock(&mtx);

	camisetas[numCam] += cantCam;

	printf("Suministradas %d camisetas del tipo %d.\n", cantCam, numCam);

	pthread_mutex_unlock(&mtx);

	pthread_exit(NULL);

}

int main()
{
	printf("\n\nNota: INICIALMENTE EXISTEN 100 CAMISETAS DE CADA TIPO\n\n");
	pthread_t thdCli[NUMCLI];
	pthread_t thdPro[NUMPRO];

	srand(time(NULL));

	int i;

	for(i = 0; i < NUMCLI; i++)
	{
		pthread_create(&(thdCli[i]), NULL, manejadorCliente, NULL);
	}


	for(i = 0; i < NUMPRO; i++)
	{
		pthread_create(&(thdPro[i]), NULL, manejadorProveedor, NULL);
	}

	for(i = 0; i < NUMCLI; i++)
	{
		pthread_join(thdCli[i], NULL);
	}

	for(i = 0; i < NUMPRO; i++)
	{
		pthread_join(thdPro[i], NULL);
	}	


	for(i = 0; i < NUMPRO; i++)
	{
		printf("Quedan del tipo [%d] un total de = %d  camisetas\n", i, camisetas[i]);
	}

	pthread_mutex_destroy(&mtx);

	pthread_exit(NULL);
}