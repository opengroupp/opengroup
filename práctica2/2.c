#include <stdio.h> 
#include <stdlib.h> 
#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

void *Sumar()
{
	int *param;

	param=(int *)malloc(sizeof(int));

	int aux1=rand()%11;
	int aux2=rand()%11;
	
	*param=aux1+aux2;

	printf("Los numero generalos son AUX1 = %d || AUX2 = %d || Y la suma TOTAL ES: -> %d.\n\n",aux1,aux2,*param);

	pthread_exit((void *)param);
}

int main(int argc, char **argv)
{
	pthread_t thd1[20];
	srand(time(NULL));
	int i, numHilos, cont=0;
	void *param;

	//Siempre que devuelva un join algo, declaramos la variable void * y ponemos & param
	
	if(argc != 2)
	{
		printf("ERROR, tiene que pasar el numero de hijos por linea de argumentos.\n");
		exit(-1);
	}
	
	for(i=0;i<atoi(argv[1]);i++)
		pthread_create (&(thd1[i]), NULL, Sumar, NULL); 
	 
	for(i=0;i<atoi(argv[1]);i++)
	{
		pthread_join(thd1[i], &param);
		cont=cont+*(int *)param;
	}
		
	printf("\nLa suma TOTAL es: %d.\n",cont);
	printf("\nHan finalizado los thread.\n");
	exit(EXIT_SUCCESS); 	
}