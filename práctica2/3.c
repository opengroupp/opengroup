#include <stdio.h> 
#include <stdlib.h> 
#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#define longVector 10

int v[longVector];

struct param
{
	int indiceVector;
	int inicioVector;
	int finVector; 
};

void *Sumar (void *Datos) 
{
	int indiceVector=((struct param *)Datos)->indiceVector;     
	int inicioVector=((struct param *)Datos)->inicioVector;
	int finVector=((struct param *)Datos)->finVector;
	int i;
	int * aux = (int*)malloc(sizeof(int));
	*aux = 0;

	printf("\nNumero de Hilo (%d) ::. Inicio (%d) -- Fin (%d).\n", indiceVector,inicioVector+1,finVector+1);

	for(i=inicioVector;i<=finVector;i++){
		*aux = *aux + v[i];
		printf("\n%d\n", v[i]);
    }
    	
	int *pid =(int *)Datos;

	printf("Numero de Hilo (%d) ::. Suma Total (%d).\n\n", indiceVector,*aux);
	pthread_exit((void*)aux); 
}


int main(int argc, char ** argv) 
{
	srand(time(NULL));

	if(argc!=2)
	{
		printf("ERROR, hay que introducir el numero de hilos por línea de argumento.\n");
		exit(EXIT_FAILURE);
	}
	
	int numeroHilos=atoi(argv[1]);

	struct param parametros[numeroHilos];
	pthread_t thd[numeroHilos];

	int i, total=0;
	void * valorDevuelto;
	int numElem=longVector/numeroHilos;   
	int resto=longVector%numeroHilos;

	printf("\nIntroducimos Valores aleatorios al vector.\n\n");
	for(i=0;i<longVector;i++)
	{
		v[i]=rand()%9+1;
		printf("v(%d) -> %d.\n",i+1,v[i]);
	} 

	for(i=0;i<numeroHilos;i++) 
	{
		parametros[i].indiceVector=i;
		parametros[i].inicioVector=i*numElem;

		if(resto!=0 && i==numeroHilos-1) 
			parametros[i].finVector=(i+1)*(numElem-1)+resto;
		else 
			parametros[i].finVector=(i+1)*(numElem)-1;

		pthread_create(&thd[i], NULL,(void *)Sumar,(void *)&parametros[i]); 
	}

	for(i=0;i<numeroHilos;i++)
	{
		pthread_join(thd[i], &valorDevuelto);
		total+= *((int *)valorDevuelto);
	}



	printf("Han finalizado los thread, la suma total es (%d).\n",total);
	pthread_exit(EXIT_SUCCESS);
}