#include <stdio.h> 
#include <stdlib.h> 
#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

void *Contar(void *cadena)
{
	char *cad = (char *)cadena;

	int *resu=(int *)malloc(sizeof(int));
	*resu = 0;

	FILE *f = fopen(cad,"r");
		if(f==NULL)
		{
			printf("Error, no existe el archivo para abrir.\n");
			exit(-1);
		}

	char caracter;
	while((caracter=fgetc(f))!=EOF)
	{
		if(caracter=='\n')
			*resu+=1;
	}

	printf("\nEl fichero tiene -> %d Lineas.\n\n",*resu);

	fclose(f);


	pthread_exit((void *)resu);
}

int main(int argc, char **argv)
{
	pthread_t thd1[9];
	int i, cont=0;
	void *param;

	//Siempre que devuelva un join algo, declaramos la variable void * y ponemos & param
	
	if(argc < 2)
	{
		printf("ERROR, tiene que pasar por argumentos al menos un fichero.\n");
		exit(-1);
	}	


	for(i=1;i<argc;i++)
		pthread_create (&(thd1[i]), NULL,Contar,(void *)argv[i]); 
	 
	for(i=1;i<argc;i++)
	{
		pthread_join(thd1[i], &param);
		cont=cont+*(int *)param;
	}
		
	printf("\nLa suma TOTAL de Lineas de los ficheros es: %d.\n",cont);
	printf("\nHan finalizado los thread.\n");
	exit(EXIT_SUCCESS); 	

return 0;
}