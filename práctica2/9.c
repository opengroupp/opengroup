#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <semaphore.h>

#define MAX_SIZE 5
#define MAX_COUNT MAX_SIZE-1
#define NUM_PROD 50

sem_t sem_buffer;
sem_t hay_sitio;
sem_t hay_datos;
int buffer[MAX_SIZE];
//int count = 0;

void * manejadorProductor(void * param)
{
	int num_items = 0;
	int * index = (int*)param;
	int pos_buffer = 0;

	while(num_items < NUM_PROD)
	{
		usleep(rand() % 100000);
		sem_wait(&hay_sitio);
		sem_wait(&sem_buffer);

		buffer[pos_buffer] = rand() % 10 + 1;
		printf("PRODUCIDO un %d\t(%d)\tHILO: %d\n", buffer[pos_buffer], num_items+1, *index);
		pos_buffer = (pos_buffer + 1) % MAX_SIZE;
		num_items++;
		sem_post(&hay_datos);

		sem_post(&sem_buffer);

	}

	printf("Ya se produjeron todos los productos.\n");

	pthread_exit(NULL);
}

void * manejadorConsumidor(void * param)
{
	int num_items = 0;
	int * index = (int *)param;
	int pos_buffer = 0;

	while(num_items < NUM_PROD)
	{
		
		usleep(rand() % 100000);
		sem_wait(&hay_datos);
		sem_wait(&sem_buffer);


		printf("Consumido un %d por el consumidor %d\t(%d).\n", buffer[pos_buffer], *index, num_items+1);
		num_items++;
		pos_buffer = (pos_buffer + 1) % MAX_SIZE;
		sem_post(&hay_sitio);

	
		sem_post(&sem_buffer);

	}

	printf("Ya se consumieron todos los productos.\n");

	pthread_exit(NULL);
}


int main()
{
	pthread_t thdProductor;
	pthread_t thdConsumidor;
	sem_init(&sem_buffer, 0, 1);
	sem_init(&hay_sitio, 0, MAX_SIZE);
	sem_init(&hay_datos, 0, 0);

	srand(time(NULL));

	int index = 0;

	pthread_create(&(thdProductor), NULL, manejadorProductor, &index);
	pthread_create(&(thdConsumidor), NULL,manejadorConsumidor, &index);

		
	pthread_join(thdProductor, NULL);
	pthread_join(thdConsumidor, NULL);



	sem_destroy(&sem_buffer);
	sem_destroy(&hay_sitio);
	sem_destroy(&hay_datos);
	pthread_exit(NULL);
}