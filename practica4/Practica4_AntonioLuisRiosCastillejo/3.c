#include<stdio.h> 

int main() 
{ 
	int i, limite=5, total=0, proc=5, aux=0, quantum=3; 
  	int t_espera=0, t_retorno=0, t_llegada[10], t_ejecucion[10], resto[10]; 
	char nombre_p[10]={'A','B','C','D','E'};

	//proceso 0
	t_llegada[0] = 0;
	t_ejecucion[0] = 8;
	resto[0] = t_ejecucion[0];

	//proceso 1
	t_llegada[1] = 1;
	t_ejecucion[1] = 4;
	resto[1] = t_ejecucion[1];

	//proceso 2
	t_llegada[2] = 2;
	t_ejecucion[2] = 9;
	resto[2] = t_ejecucion[2];

	//proceso 3
	t_llegada[3] = 3;
	t_ejecucion[3] = 5;
	resto[3] = t_ejecucion[3];

	//proceso 4
	t_llegada[4] = 4;
	t_ejecucion[4] = 2;
	resto[4] = t_ejecucion[4];

	
     	printf("\nProceso\t\tTiempo de Ejecucion\t Tiempo de Retorno\t Tiempo de Espera\n");

	for(total=0,i=0;proc!=0;) 
  	{ 
    		if(resto[i]<=quantum && resto[i]>0) 
    		{ 
      			total = total + resto[i]; 
      			resto[i] = 0; 
      			aux = 1; 
    		} 
    		else if(resto[i]>0) 
    		{ 
      			resto[i]=resto[i]-quantum; 
      			total=total+quantum; 
    		} 

	    		if(resto[i]==0 && aux==1) 
	    		{ 
	      			proc--; 

	 			printf("\nProceso -> %c \t\t %d \t\t \t%d \t\t\t %d", nombre_p[i], t_ejecucion[i], total-t_llegada[i], total-t_llegada[i]-t_ejecucion[i]);

	      			t_espera=t_espera+total-t_llegada[i]-t_ejecucion[i];
	 
	      			t_retorno=t_retorno+total-t_llegada[i];
	 
	      			aux = 0; 
	    		} 

    		if(i==limite-1) 
			i = 0;
    		else if(t_llegada[i + 1]<=total) 
			i++;
    		else 
			i=0;
	}
	
	printf("\n\n");
  	return 0; 
}
