Fork()

los exit(dato) devuelve el estado en el que ha terminado a su padre.

****Los procesos padres e hijos no comparten memoria****

Una vez que se realiza el fork los procesos se encuentran en una zona distinta de memoria.
***Si uno vale 5 y otro 10, hacemos printf y cada uno de ellos vale su mismo numero***

-------------------------------------------------------------------------------------------------
Memoria Compartida

1º--> Crear la zona de memoria  

	#include <sys/shm.h>
	int shmget (key, size, flag);
	key_t key;	  /* clave del segmento de memoria compartida */
	int size; 	  /* tamaño del segmento de memoria compartida */
	int flag;	  /* opción para la creación */



shmat(shmid, NULL, 0);


2º-->

	#define LLAVE (key_t) 234 /* clave de acceso */
	int shmid; /* identificador del nuevo segmento de memoria compartida */
	if((shmid=shmget(LLAVE, 4096, IPC_CREAT | 0600)) == -1)
	{
	/* Error al crear o habilitar el segmento de memoria compartida. Tratamiento del error. */
	}


3º-->Conexión a un segmento de memoria compartida

	#include <sys/shm.h>
	char *shmat (shmid, *shmadr, shmflag);
	int shmid;	/* identificador del segmento */
	char *shmadr;	/* dirección de enlace */
	int flag;	/* opción de conexión */
	
	
	

4º--> Desconexión a un segmento de memoria compartida

	#include <sys/shm.h>
	int shmdt (*shmadr);
	char *shmadr; /* dirección de conexión del segmento a desenlazar */


5º--> Desconexión a un segmento de memoria compartida


	#include <sys/shm.h>
	int shmdt (*shmadr);
	char *shmadr; /* dirección de conexión del segmento a desenlazar */



6º--> Control de un segmento de memoria compartida

	
	#include <sys/shm.h>
	int shmctl (shmid, cmd, *buf);
	int shmid;	/* identificador del segmento */
	int cmd;	/* operación a efectuar */
	struct shnud_ds *buf;		/* información asociada al segmento de memoria compartida */
	
	


EJercicio 7
---------------------------------------------------------------------------------------------

*contador=0;








