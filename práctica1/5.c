#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	int i;
	int pid;
	int estado;
	int bandera;

	if(argc != 2)
	{
		printf("\n\nError, se necesita el nombre del fichero.\n\n");
		exit(EXIT_FAILURE);
	}

	FILE *fp;
	fp=fopen(argv[1],"w");

	for(i=0;i<2;i++)
	{
		pid=fork();

		if(pid==0)
		{
			if(i==0)	//Primer hijo
			{
				fprintf(fp,"Hijo 1  ----- \n");
				sleep(1);
				exit(EXIT_SUCCESS);
			}	
				
			else if(i==1)	//Segundo hijo
			{
				fprintf(fp,"Hijo 2 _ _ _ _ _ _ \n\n");
				sleep(2);
				exit(EXIT_SUCCESS);
			}
				
		}
		else if(pid<0)
			exit(EXIT_FAILURE);
				
	}

	fprintf(fp," \nPadre +++++ \n");
	sleep(1);
	exit(EXIT_SUCCESS);
	
	while((bandera=wait(&estado)) > 0)
	{
		if(WIFEXITED(estado))
			printf("Finaliza de formal normal y devuelve: (%d).\n\n",WEXITSTATUS(estado));
		else if(WIFSIGNALED(estado))
			printf("Finaliza por recibir una señal y devuelve: (%d).\n\n", WTERMSIG(estado));
		else if(WIFSTOPPED(estado))
			printf("Proceso parado, devuelve: (%d).\n\n", WSTOPSIG(estado));		
		else if(WIFCONTINUED(estado))
			printf("Proceso reanudado.\n\n");
	}
	
	fclose(fp);
	return 0;
}