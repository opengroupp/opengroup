#include <sys/types.h> //Para estructura pid_t 
#include <sys/wait.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> //Para fork()
#include <errno.h>

int main(int argc, char **argv)
{
	int i;
	int estado_hijo;
	int pid[30];

if(argc != 2)
	{
		printf("\n\nError, se necesita pasar un argumento que indica el numero de procesos.\n\n");
		exit(EXIT_FAILURE);
	}

	printf("\n");


for(i=0;i<atoi(argv[1]);i++)
	{
		pid[i]=fork();
		
		if(pid[i]==0)
		{
			printf("PROCESO NUMERO %d ))--> Proceso hijo con PID -> %d, su padres es -> %d.\n ",i+1,getpid(),getppid());
			sleep(4);
			exit(EXIT_SUCCESS);
		}
		else if(pid[i]<0)
		{
			printf("ERROR, al crear el proceso.\n");
			exit(EXIT_FAILURE);
		}
	}
	
	printf("\n\n");

	for(i=0;i<atoi(argv[1]);i++)
	{
		pid[i]=wait(&estado_hijo);

			printf("Proceso[PID] -> %d ", pid[i]);
		
			if(WIFEXITED(estado_hijo))
				printf("Finaliza de formal normal y devuelve: (%d).\n\n",i);
			else if(WIFSIGNALED(estado_hijo))
				printf("Finaliza por recibir una señal y devuelve: (%d).\n\n", WTERMSIG(estado_hijo));
			else if(WIFSTOPPED(estado_hijo))
				printf("Proceso Parado, devuelve: (%d).\n\n", WSTOPSIG(estado_hijo));		
			else if(WIFCONTINUED(estado_hijo))
				printf("Proceso Reanudado.\n\n");		

	}
	
	return 0;
}

//ES MAS EFICIENTE HACERLO EN DOS FOR, PUESTO QUE SI LO HICIESEMOS TODO EN UN FOR, SERIA SECUENCIA Y LO QUE BUSCASCAMOS
//EN LOS SISTEMAS OPERATIVOS ES EL ASINCRONISMO.