#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
	pid_t p1, p2, p3, p4, p5;
	int digito, status;
	int suma = 0;

	printf("Soy el padre, mi pid es %d\n", getpid());

	p1 = fork();
	if(p1 == 0)
	{
		
		digito = getpid() % 10;
		printf("Soy el hijo 1, mi pid es %d mi padre es %d y mi suma es %d\n", getpid(),getppid(), digito);
		exit(digito);
	}

	p2 = fork();
	if(p2 == 0)
	{
		digito = getpid() % 10;
		printf("Soy el hijo 2, mi pid es %d , mi padre es %d y mi suma es %d\n", getpid(),getppid(),digito);

		p3 = fork();

		if(p3 == 0) //Hijo 3
		{
			
			digito = getpid() % 10;
			printf("Soy el hijo 3, mi pid es %d mi padre es %d y mi suma es %d\n", getpid(),getppid(), digito);
			exit(digito);
		}

		p4 = fork();

		if(p4 == 0) //Hijo 4
		{
			digito = getpid() %10;
			printf("Soy el hijo 4, mi pid es %d mi padre es %d y mi suma es %d\n", getpid(),getppid(),digito);

			p5 = fork();

			if(p5 == 0) //Hijo 5
			{
				
				digito = getpid() % 10;
				printf("Soy el hijo 5, mi pid es %d , mi padre es %d y mi suma es %d\n\n", getpid(),getppid(), digito);
				exit(digito);

			}

			while(wait(&status) != -1)
			{
				suma += WEXITSTATUS(status);
			}

			digito = getpid() % 10;
			exit(digito + suma);

		}

		while(wait(&status) != -1)
		{
			suma += WEXITSTATUS(status);
		}
		digito = getpid() % 10;
		exit(digito + suma);

	}
	

	while(wait(&status) != -1)
	{
		suma += WEXITSTATUS(status);
	}

	suma += getpid() % 10;

	

	printf("La suma total es %d\n\n\n", suma);

	return 0;

}