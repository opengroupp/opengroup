#include <sys/types.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>


#define NPROC 500
#define NITER 100

int main(int argc, char **argv)
{
	int i,j, Clave, Clave2;
	int estado, bandera;
	int pid;
	int Id_Memoria, Id_Token;
	int *contador = NULL, *token = NULL;


	//Paso 1 Clave para Memoria[i]

	Clave = ftok (".", 33);

	if (Clave == -1)
	{
		printf("No consigo clave para Memoria compartida\n");
		exit(0);
	}


	Id_Memoria = shmget (Clave, sizeof(int)*100, 0777 | IPC_CREAT);
	if (Id_Memoria == -1)
	{
		printf("No consigo Id para Memoria compartida\n");
		exit (0);
	}

			//Paso 1.1
			Clave2 = ftok (".", 14);

			if (Clave2 == -1)
			{
				printf("No consigo clave para Memoria compartida para el TOKEN\n");
				exit(0);
			}


			Id_Token = shmget (Clave2, sizeof(int)*100, 0777 | IPC_CREAT);
			if (Id_Token == -1)
			{
				printf("No consigo Id para Memoria compartida TOKEN\n");
				exit (0);
			}

	//Paso 2 Memoria compartida para Memoria[i]
	contador = (int *)shmat (Id_Memoria, (char *)0, 0);
	if (contador == NULL)
	{
		printf("No consigo Memoria compartida");
		exit (0);
	}
	
	//Paso 2.1
	token = (int *)shmat (Id_Token, (char *)0, 0);
	if (token == NULL)
	{
	printf("No consigo Memoria compartida");
		exit (0);
	}


	*contador=0;
	*token=0;

	//Paso 3
	for(i=0;i<NPROC;i++)
	{
		pid=fork();

			if(pid==0)
			{
				
				while(*token!=i)
				{
					usleep(1);	
				}
					
				for(j=0;j<NITER;j++)
				{
					contador[0]++;
				}
				
				(*token)++;
		
				exit(EXIT_SUCCESS);

			}
			else if(pid<0)
			{
				printf("Error, no se ha podido crear el proceso.\n\n");
				exit(EXIT_FAILURE);
			}
		
		//(*token)++;
	}

	while((bandera=wait(&estado)) > 0)
	{
		if(WIFEXITED(estado))
			printf("Finaliza de formal normal y devuelve: (%d).\n\n",WEXITSTATUS(estado));
		else if(WIFSIGNALED(estado))
			printf("Finaliza por recibir una señal y devuelve: (%d).\n\n", WTERMSIG(estado));
		else if(WIFSTOPPED(estado))
			printf("Proceso parado, devuelve: (%d).\n\n", WSTOPSIG(estado));		
		else if(WIFCONTINUED(estado))
			printf("Proceso reanudado.\n\n");
	}

	printf("\n\nEl resultado del contador es -> %d.\n\n",contador[0]);

	
	//Paso 4 
	shmdt ((char *)contador);
	shmctl (Id_Memoria, IPC_RMID, (struct shmid_ds *)NULL);

	//Paso 4.1
	shmdt ((char *)token);
	shmctl (Id_Token, IPC_RMID, (struct shmid_ds *)NULL);
	

	return 0;
}