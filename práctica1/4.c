#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

int funcionCalculadora(char * parametro);
int funcionGedit(char ** parametros);

int main(int argc, char **argv)
{
	int i;
	int estado;
	int pid[2];
	int bandera;

	if(argc < 3)
	{
		printf("\n\nError, se necesita pasar un minimo de 3 procesos(gnome-calculator gedit <fich1.txt> <fich2.txt> <fich_N.txt>).\n\n");
		exit(EXIT_FAILURE);
	}

		for(i=0;i<2;i++)
		{
			pid[i]=fork();

			if(pid[i]==0)
			{
				if(i==0)	//cada iteracion del for nos dira el hijo.
					funcionCalculadora(argv[1]);
				else if(i==1)
					funcionGedit(&argv[2]);
			}
			else if(pid<0)
				exit(EXIT_FAILURE);
				
		}


	while((bandera=wait(&estado)) > 0)
	{
		if(WIFEXITED(estado))
			printf("Finaliza de formal normal y devuelve: (%d).\n\n",WEXITSTATUS(estado));
		else if(WIFSIGNALED(estado))
			printf("Finaliza por recibir una señal y devuelve: (%d).\n\n", WTERMSIG(estado));
		else if(WIFSTOPPED(estado))
			printf("Proceso parado, devuelve: (%d).\n\n", WSTOPSIG(estado));		
		else if(WIFCONTINUED(estado))
			printf("Proceso reanudado.\n\n");
	}

	return 0;
}

int funcionCalculadora(char * parametro)
{	
	execlp(parametro, parametro, NULL);
	return 0;
}

int funcionGedit(char ** parametros)
{	
	execvp(*parametros, parametros);
	return 0;
}


