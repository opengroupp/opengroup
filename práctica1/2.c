#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

int main(int argc, char **argv)
{

	int pid;

	pid = fork();


	if(pid==0){

		printf("PROCESO ))--> Proceso hijo con PID -> %d, su padre es -> %d.\n ",getpid(),getppid());
		exit(EXIT_SUCCESS);
	}
	else{

		sleep(10);
		exit(EXIT_SUCCESS);
	}

}