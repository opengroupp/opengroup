#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	int i, childpid;
	int estado_hijo;
	int pid[30];

	if(argc != 2)
	{
		printf("\n\nError, se necesita pasar un argumento que indica el numero de procesos.\n\n");
		exit(EXIT_FAILURE);
	}

	printf("\n\n");

	for(i=0;i<atoi(argv[1]);i++)
	{
		pid[i]=fork();
		
		if(pid[i]>0)
		{

			childpid=waitpid(-1,&estado_hijo,0);

			if(childpid>0)
			{
				if (WIFEXITED(estado_hijo)) {
				    printf("child %d exited, status=%d\n",childpid, WEXITSTATUS(estado_hijo));
				} else if (WIFSIGNALED(estado_hijo)) {
				    printf("child %d killed (signal %d)\n", childpid, WTERMSIG(estado_hijo));
				} else if (WIFSTOPPED(estado_hijo)) {
				    printf("child %d stopped (signal %d)\n", childpid, WSTOPSIG(estado_hijo));
				} 
		    }


		    else{

		    	printf("ERROR, al crear el proceso.\n");
				exit(EXIT_FAILURE);
		    }

		    exit(EXIT_SUCCESS);			
			
		}

		else{

			printf("PROCESO NUMERO %d ))--> Proceso hijo con PID -> %d, su padres es -> %d.\n ",i+1,getpid(),getppid());
		}
		
	}
	
	printf("\n\n");

	
	
	return 0;
}
