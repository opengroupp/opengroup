#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

int vg=0;

int main(int argc, char **argv)
{

	int i;
	int estado;
	int pid;
	int bandera;


	for(i=0;i<3;i++)
	{
		pid=fork();

		if(pid==0)
		{
			vg=vg+1;
			printf("PROCESO ((i del for-> %d ))--> [ PADRE-> %d ], [ HIJO -> %d ].\n ",i,getppid(),getpid());
			sleep(5);
			exit(EXIT_SUCCESS);
			
		}
		else if(pid<0)
		{
			printf("Error al crear el proceso.\n\n");
			exit(EXIT_FAILURE);
		}
	}



	while((bandera=wait(&estado)) > 0)
	{
		if(WIFEXITED(estado))
			printf("Finaliza de formal normal y devuelve: (%d).\n\n",WEXITSTATUS(estado));
		else if(WIFSIGNALED(estado))
			printf("Finaliza por recibir una señal y devuelve: (%d).\n\n", WTERMSIG(estado));
		else if(WIFSTOPPED(estado))
			printf("Proceso parado, devuelve: (%d).\n\n", WSTOPSIG(estado));		
		else if(WIFCONTINUED(estado))
			printf("Proceso reanudado.\n\n");
	}


	printf("SOY EL --> [ PADRE-> %d ], y la suma de la variable GLOBAL es -> %d.\n ",getpid(),vg);
	sleep(1);

	return 0;
}